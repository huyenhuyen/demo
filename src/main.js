// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// Thiet ke giao dien vs Vuetify co the thay = Bootstrap vue
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'

//dung lodash de load array nhanh
import upperFirt from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

//Dung firebase de xac thuc quyen
import firebase from 'firebase'
import { config } from './firebase/config'

import router from './router'
import Vue from 'vue'
import App from './App'

Vue.use(Vuetify);
Vue.use(require('vue-moment'));

Vue.config.productionTip = false

const requirmentComponent = require.context(
	'./components/base' , false, /Base[\w-]+\.vue$/,
	);

requirmentComponent.keys()
.forEach( filename => {
	const componentConfig = requirmentComponent(filename);
	const componentName = upperFirt(
		camelCase(filename.replace(/^\.\//, '').replace(/\.\w+$/, '')),
		);

Vue.component(componentName, componentConfig.default || componentConfig );

});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  created(){
  	firebase.initializeApp(config);
  },
  components: { App },
  template: '<App/>'
})
